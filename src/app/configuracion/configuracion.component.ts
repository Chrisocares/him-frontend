import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DeleteDialogComponent } from '../dialogs/delete/delete.dialog.component';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { ConfigurationService } from '../service/configuration.service';
import { ConfirmacionDialogComponent } from '../dialogs/confirmacion-dialog/confirmacion-dialog.component';
import { ApplicationService } from '../service/application.service';


@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.component.html',
  styleUrls: ['./configuracion.component.css']
})
export class ConfiguracionComponent implements OnInit {

  displayedColumns = ['orden', 'programa', 'fecha', 'ambiente', 'actions'];
  dataSource;

  lastUpdate: string;
  checkStatePost;
  checkedUpdates;
  checkedUpdatesSlide;
  isChecked = true;
  formGroup: FormGroup;

  constructor(
    public aplicationService: ApplicationService,
    public configurationService: ConfigurationService,
    public dialog: MatDialog,
    formBuilder: FormBuilder) {
    this.formGroup = formBuilder.group({
      enableWifi: '',
      acceptTerms: ['', Validators.requiredTrue]
    });
  }

  ngOnInit() {
    this.configurationService.getConfiguration("5ef1478bef98d145f89a68f1").then(response => {
      this.lastUpdate = response.last_update;
      this.checkedUpdatesSlide = response.updates
      if (response.updates == true) {
        this.checkedUpdates = false;
      } else {
        this.checkedUpdates = true;
      }
      this.dataSource = new MatTableDataSource(response.updates_battery.filter(
        updates => updates.pending_approve === true
      ));
    })
  }

  aprovarCambio(item: any) {
    console.log(JSON.stringify(item))
    const body = { 
      "programa": item.programa,
      "nombre_copy": "",
      "funcionalidad": "",
      "entrada": "",
      "salida": "",
      "bus_codigo": "",
      "bus_version": "",
      "bus_framework": "",
      "bus_identificador": "",
      "api": "",
      "new": true,
    }
    this.aplicationService.postProgram("5eef0c5aef62490012902e70",body).then(response => {
      const dialogRef = this.dialog.open(ConfirmacionDialogComponent, {
        width: '30%',
        height: '25%',
        data: {
          title: "Nuevo Programa",
          subtitle: "Se agrego el programa "+item.programa +" exitosamente",
          accion: "Cerrar"
        }
      });
    });

  }

  actualizarConfiguracion() {
    if (this.checkStatePost != null) {
      const body = { "updates": this.checkStatePost}
      this.configurationService.postConfiguration("5ef1478bef98d145f89a68f1", body).then(response => {
        if(response.updates == false){
          this.checkedUpdates = true;
          const dialogRef = this.dialog.open(ConfirmacionDialogComponent, {
            width: '30%',
            height: '30%',
            data: {
              title: "Configuración",
              subtitle: "Se deshabilito correctamente las actualizaciones automaticas",
            }
          });
        } else {
          this.checkedUpdates = false;
          const dialogRef = this.dialog.open(ConfirmacionDialogComponent, {
            width: '30%',
            height: '30%',
            data: {
              title: "Configuración",
              subtitle: "Se habilitaron correctamente las actualizaciones automaticas",
            }
          });
        }
      })
    }
  }

  onChangeSlideToggle(event: any) {
    this.checkStatePost = event.checked
    console.log(this.checkStatePost)
  }
}

export interface PeriodicElement {
  fecha: string;
  orden: number;
  ambiente: string;
}
