import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from './material/material.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { HomeComponent } from './home/home.component';
import { MenuComponent } from './menu/menu.component';
import * as $ from 'jquery';

// Import HttpClientModule from @angular/common/http
import {HttpClientModule} from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SafePipe } from './safe.pipe';
import { FilterPipe }  from './filter.pipe';
import { AplicacionComponent } from './aplicacion/aplicacion.component';
import { ProgramaComponent } from './programa/programa.component';
import { AddDialogComponent } from './dialogs/add/add.dialog.component';
import { EditDialogComponent } from './dialogs/edit/edit.dialog.component';
import { DeleteDialogComponent } from './dialogs/delete/delete.dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import { InformacionDialogComponent } from './dialogs/informacion/informacion.dialog.component';
import { AddProgramaDialogComponent } from './dialogs/add-programa-dialog/add-programa-dialog.component';
import { DeleteProgramaDialogComponent } from './dialogs/delete-programa-dialog/delete-programa-dialog.component';
import { EditProgramaDialogComponent } from './dialogs/edit-programa-dialog/edit-programa-dialog.component';
import { InformacionProgramaDialogComponent } from './dialogs/informacion-programa-dialog/informacion-programa-dialog.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { LoginComponent } from './login/login.component';
import { MatRippleModule, MatSidenavModule } from '@angular/material';
import { InformacionServicioDialogComponent } from './dialogs/informacion-servicio-dialog/informacion-servicio-dialog.component';
import { XmlPipe } from './xml.pipe';
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { AddUserComponent } from './dialogs/add-user/add-user.component';
import { InformacionUsuarioComponent } from './dialogs/informacion-usuario/informacion-usuario.component';
import { ConfirmacionDialogComponent } from './dialogs/confirmacion-dialog/confirmacion-dialog.component';

@NgModule({
  declarations: [
    XmlPipe,
    FilterPipe,
    FileUploadComponent,
    AppComponent,
    AddDialogComponent,
    EditDialogComponent,
    DeleteDialogComponent,
    InformacionDialogComponent,
    InformacionServicioDialogComponent,
    SidenavComponent,
    HomeComponent,
    LoginComponent,
    MenuComponent, 
    SafePipe, AplicacionComponent, ProgramaComponent, AddProgramaDialogComponent, DeleteProgramaDialogComponent, EditProgramaDialogComponent, InformacionProgramaDialogComponent, FileUploadComponent, LoginComponent, InformacionServicioDialogComponent, ConfiguracionComponent, UsuarioComponent, AddUserComponent, InformacionUsuarioComponent, ConfirmacionDialogComponent
  ],
  imports: [
    MatSidenavModule,
    MatRippleModule,
    FlexLayoutModule,
    BrowserModule,
    AppRoutingModule, 
    MaterialModule, 
    HttpClientModule, 
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    AddDialogComponent,
    EditDialogComponent,
    DeleteDialogComponent,
    InformacionDialogComponent,
    InformacionServicioDialogComponent,
    InformacionUsuarioComponent,
    AddProgramaDialogComponent,
    DeleteProgramaDialogComponent,
    EditProgramaDialogComponent,
    InformacionProgramaDialogComponent,
    AddUserComponent,
    ConfirmacionDialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
