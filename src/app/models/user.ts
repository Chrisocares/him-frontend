export interface User {
    ok: string;
    usuario : {
        role : string;
        estado : string;
        _id : string;
        nombre : string;
        email : string;
        password : string;
        access : string[];
        __v : string;
    };
    token : string;
}