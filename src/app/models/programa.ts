export interface Programa {
    _id: number;
    nombre_copy: string;
    programa: string;
    funcionalidad: string;
    entrada: string;
    salida: string;
    bus_codigo: string;
    bus_version: string;
    bus_framework: string;
    bus_identificador: string;
    api: string;
  }