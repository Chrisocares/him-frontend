import { Programa } from './programa';

export interface Aplicacion {
  id: number;
  nombre: string;
  objetivo: string;
  custodio: string;
  plataforma: string;
  tipo: string;
  programas : Array<Programa>
}