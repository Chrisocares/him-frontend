export interface Configuration {
  _id: number;
  updates: Boolean;
  last_update: string;
  custodio: string;
  updates_battery: Array<updates_battery>;
}

export interface updates_battery {
    _id: number;
    pending_approve: Boolean;
    nombre_copy: string;
  }