import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  SERVER_URL: string = "http://localhost:3000/";
  constructor(private http: HttpClient) { }


  public formatErrors(error: HttpErrorResponse) {
    return throwError(error.error ? error.error : error);
  }

  get(path: string): Observable<any> {
    return this.http.get(path).pipe(catchError(this.formatErrors));
  }

  getBlob(path: string): Observable<any> {
    return this.http.get(path, { responseType: "blob" }).pipe(catchError(this.formatErrors));
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.http
      .put(path, JSON.stringify(body))
      .pipe(catchError(this.formatErrors));
  }

  post(path: string, body: Object = {}): Observable<any> {

    return this.http
      .post(path, JSON.stringify(body), { headers: new HttpHeaders({'Content-Type': 'application/json'})})
      .pipe(catchError(this.formatErrors));
  }

  delete(path): Observable<any> {
    return this.http.delete(path).pipe(catchError(this.formatErrors));
  }

  postForm(path: string, body: string): Observable<any> {
    return this.http
      .post(path, body, { headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded') })
      .pipe(catchError(this.formatErrors));
  }

}
