import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { DataService } from './data.service';
import { map } from "rxjs/operators";
import { Aplicacion } from '../models/aplicacion';
import { Configuration } from '../models/configuration';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  constructor(
    private dataService: DataService,
    private http: HttpClient) { }

  async getConfiguration(id: string): Promise<Configuration> {
    const configuration: Configuration = await this.dataService
      .get("http://localhost:3000/updates/"+id).pipe(map(response => JSON.parse(JSON.stringify(response))["configurations"]))
      .toPromise();
    return configuration;
  }

  async postConfiguration(id: string, body: any): Promise<Configuration> {
    const configuration: Configuration = await this.dataService
      .post("http://localhost:3000/updates/"+id, body).pipe(map(response => JSON.parse(JSON.stringify(response))["configurations"]))
      .toPromise();
    return configuration;
  }

}