import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DataService } from './data.service';
import { User } from '../models/user';
import { HttpParams } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class AuthService {
  private loggedIn = new BehaviorSubject<boolean>(false);
  private userIdentity = new BehaviorSubject<any>({});

  constructor(
    private dataService: DataService
  ) {
    this.loggedIn.next(this.isAuthenticated());
    this.userIdentity.next(this.getUser());
  }

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  get user() {
    return this.userIdentity.asObservable();
  }

  async login(body: any): Promise<User> {
    const login = new HttpParams()
        .set('email', body.username.toLowerCase())
        .set('password', body.password);
    const user: User = await this.dataService
      .postForm(this.dataService.SERVER_URL+"login", login.toString())
      .toPromise();
    this.setSession({ user });
    return user;
  }

  logout() {
    this.loggedIn.next(false);
    localStorage.removeItem("him-auth");
  }
  
  getUser(): User {
    if (localStorage.getItem("him-auth")) {
      const session = JSON.parse(localStorage.getItem("him-auth"));
      return session.user;
    } else {
      return null;
    }
  }

  private setSession(session: any): void {
    session.expires_in = 1000 * 60 * 60 * 24 + new Date().getTime();
    localStorage.setItem("him-auth", JSON.stringify(session));
  }

  public isAuthenticated(): boolean {
    if (localStorage.getItem("him-auth")) {
      const session = JSON.parse(localStorage.getItem("him-auth"));
      if (new Date().getTime() < session.expires_in) {
        return true;
      } else {
        this.logout();
        return false;
      }
    } else {
      return false;
    }
  }
}
