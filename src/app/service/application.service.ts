import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { DataService } from './data.service';
import { Application } from '../models/application.model';
import { map } from "rxjs/operators";
import { Aplicacion } from '../models/aplicacion';
import { saveAs } from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  constructor(
    private dataService: DataService,
    private http: HttpClient) { }

  async getApplications(): Promise<Array<Aplicacion>> {
    const applications: Array<Aplicacion> = await this.dataService
      .get("http://localhost:3000/apps").pipe(map(response => JSON.parse(JSON.stringify(response))["applications"]))
      .toPromise();
    return applications;
  }

  async getApplication(id: string): Promise<Aplicacion> {
    const application: Aplicacion = await this.dataService
      .get("http://localhost:3000/AppById/" + id).pipe(map(response => JSON.parse(JSON.stringify(response))["applications"]))
      .toPromise();
    return application;
  }

  async getApplicationDownload(): Promise<any> {
    const response = await this.dataService
      .getBlob("http://localhost:3000/apps/download")
      .toPromise();
    return response;
  }

  async getProgramsDownload(): Promise<any> {
    const response = await this.dataService
      .getBlob("http://localhost:3000/programs/download")
      .toPromise();
    return response;
  }

  async postProgram(id: string,body: Object = {}): Promise<any> {
    const response = await this.dataService
      .post("http://localhost:3000/apps/program/"+id,body)
      .toPromise();
    return response;
  }

}