import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DataService } from './data.service';
import { User } from '../models/user';
import { HttpParams } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';

@Injectable({ providedIn: 'root' })
export class UserService {
  private loggedIn = new BehaviorSubject<boolean>(false);
  private userIdentity = new BehaviorSubject<any>({});

  constructor(
    private dataService: DataService
  ) {

  }

  async register(body: any): Promise<User> {
      
    const user = new HttpParams()
        .set('nombre', body.nombre)
        .set('email', body.usuario)
        .set('crew', body.crew)
        .set('password', body.password)
        .set('role', body.permisos)
        .set('estado', "true")
        .set('rolInterbank', body.rol)
        .set('access', "RM,SIEBEL");
        console.log(JSON.stringify(user))
    const response: User = await this.dataService
      .postForm(this.dataService.SERVER_URL+"users", user.toString())
      .toPromise();
      
    return response;
  }

  async getUsers(): Promise<Array<User>> {
    const users: Array<User> = await this.dataService
    .get("http://localhost:3000/users").pipe(map(response => JSON.parse(JSON.stringify(response))["users"]))
    .toPromise();
    return users;
  }

  async deleteUser(id: string): Promise<any> {
    const response = this.dataService.delete("http://localhost:3000/users/"+id).toPromise();
    return response;
  }

}
