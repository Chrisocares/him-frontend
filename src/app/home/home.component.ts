import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Aplication } from '../models/aplication';
import { AddDialogComponent } from '../dialogs/add/add.dialog.component';
import { MatDialog, MatSidenav } from '@angular/material';
import { EditDialogComponent } from '../dialogs/edit/edit.dialog.component';
import { InformacionDialogComponent } from '../dialogs/informacion/informacion.dialog.component';
import { DataService } from '../service/data.service';
import { HttpClient } from '@angular/common/http';
import { ApplicationService } from '../service/application.service';
import { Aplicacion } from '../models/aplicacion';
import { AuthService } from '../service/auth.service';
import { saveAs } from 'file-saver';

interface TypesApplication {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  list = new Array<Aplicacion>();
  defaultElevation = 2;
  raisedElevation = 8;
  data: any;
  private items: Aplication[] = [];
  aplicacionHost : Aplication;
  typesApplications: TypesApplication[] = [
    {value: '1', viewValue: 'Host'},
    {value: '2', viewValue: 'Distribuido'}
  ];

  constructor(public authService: AuthService, public dialog: MatDialog, private dataService: DataService, private http: HttpClient, private applicationService: ApplicationService) { }

  displayedColumns: string[] = ['nombre', 'objetivo', 'custodio', 'plataforma'];
  access: string[] = [];
  dataSource = new MatTableDataSource(this.items);
  roleUser: string;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    this.applicationService
    .getApplications().then(response => {
      this.list = response;
    })
    this.roleUser = this.authService.getUser().usuario.role
    this.access = this.authService.getUser().usuario.access;
  }

  selectTypeApplication(i: string){
    this.applicationService
    .getApplications().then(response => {
      this.list = response.filter(obj => obj.tipo == i);
    })
  }

  descargar(nombre: string) {
    this.applicationService.getApplicationDownload().then(response => {
      saveAs(response, nombre+".xlsx");
    }).catch(e => {
      console.log(e);
   });
  }

}
