import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AplicacionComponent } from './aplicacion/aplicacion.component';
import { ProgramaComponent } from './programa/programa.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { UsuarioComponent } from './usuario/usuario.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  }, 
  {
    path: 'home',
    component: HomeComponent
  }, 
  {
    path: 'aplicacion/:id',
    component: AplicacionComponent
  }, 
  {
    path: 'programa/:nombre_copy',
    component: ProgramaComponent
  },
  {
    path: 'configuracion',
    component: ConfiguracionComponent
  },
  {
    path: 'usuarios',
    component: UsuarioComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
