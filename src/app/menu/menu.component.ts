import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  flag = false;
  
  @Output() cambioDeTamanioFlag: EventEmitter<boolean> = new EventEmitter<boolean>();
  
  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon('tarjetaCredito', sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/Icono_membresia.svg'));
    iconRegistry.addSvgIcon('tarjetaDebito', sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/Icono_debitcard.svg'));
    iconRegistry.addSvgIcon('prestamo', sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/Icono_Prestamo.svg'));
    iconRegistry.addSvgIcon('seguros', sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/Icono_seguros.svg'));
    iconRegistry.addSvgIcon('campanas', sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/Icono_te ofrecemos.svg'));
  }

  ngOnInit() {
  }

  hideShowMenu() {
    this.flag = !this.flag;
    this.cambioDeTamanioFlag.emit(this.flag);
  }
}
