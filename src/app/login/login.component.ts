import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public isSubmitting = false;
  show: boolean = true

  public form: FormGroup;

  constructor(
    public router : Router,
    formBuilder: FormBuilder,
    public authService: AuthService 
  ) { 
    this.form = formBuilder.group({
      username: [
        null,
        [
          Validators.required,
          Validators.minLength(4)
        ]
      ],
      password: [
        '',
        [Validators.required, Validators.minLength(4), Validators.maxLength(20)]
      ]
    });
  }

  ngOnInit() { }

  onSubmit() {
    this.isSubmitting = true;
    this.form.get('username').setValue(this.form.get('username').value.toUpperCase());
    this.authService
      .login(this.form.value)
      .then(user => {
        this.isSubmitting = false;
        this.router.navigate(['/']);
      })
      .catch(error => {
        this.isSubmitting = false;
        //this.error = error;
      });
  }

}
