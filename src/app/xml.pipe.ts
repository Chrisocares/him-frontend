import { Pipe, PipeTransform } from '@angular/core';
import * as vkbeautify from 'vkbeautify';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl  } from '@angular/platform-browser';

@Pipe({
  name: 'xml'
})
export class XmlPipe implements PipeTransform {

  constructor(protected sanitizer: DomSanitizer) {}
 
  public transform(value: any, type: string): string {
	return vkbeautify.xml(value);
	}
}