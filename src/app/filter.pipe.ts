import { Pipe, PipeTransform } from '@angular/core';
import { Aplicacion } from './models/aplicacion';
const { isArray } = Array;

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(posts: Aplicacion[], find: string): Aplicacion[] {
    if(!posts) return [];
    if(!find) return posts;
    find = find.toString().toLowerCase();
    return search( posts, find);
   }
}

function search(entries: any[], search: string) {
  search = search.toString().toLowerCase();

  return entries.filter(function (obj) {
    const keys: string[] = Object.keys(obj);
    return keys.some(function (key) {
      const value = obj[key];
      if (isArray(value)) {
        return value.some(v => {
          return v.toString().toLowerCase().includes(search);
        });
      }
      else if (!isArray(value)) {
        return value.toString().toLowerCase().includes(search);
      }
    })
  });
}