import { Component, OnInit, ViewChild } from '@angular/core';

import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { MatSidenav } from '@angular/material';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {

  @ViewChild('sidenav', { static: true }) sidenav: MatSidenav;
  isExpanded = false;
  showSubmenu: boolean = false;
  isShowing = false;
  showSubSubMenu: boolean = false;
  show: boolean = false;

  constructor(private authService: AuthService, private iconRegistry: MatIconRegistry, private sanitizer: DomSanitizer) {
    this.iconRegistry.addSvgIcon('chatNav', sanitizer.bypassSecurityTrustResourceUrl('assets/img/icons/Icono_chat_blanco.svg'));
  }

  ngOnInit() {
    var role = this.authService.getUser().usuario.role;
    if (role === "ORG_ADMIN_ROLE") {
      this.show = true;
    }
  }

}
