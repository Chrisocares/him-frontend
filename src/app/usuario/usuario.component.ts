import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DeleteDialogComponent } from '../dialogs/delete/delete.dialog.component';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { InformacionUsuarioComponent } from '../dialogs/informacion-usuario/informacion-usuario.component';
import { AddUserComponent } from '../dialogs/add-user/add-user.component';
import { UserService } from '../service/user.service';
import { User } from '../models/user';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  displayedColumns = ['nombre', 'crew', 'email', 'rolInterbank', 'actions'];
  dataSource;

  isChecked = true;
  formGroup: FormGroup;
  
  constructor(
    public userService: UserService,
    public dialog: MatDialog,
    formBuilder: FormBuilder) {
    this.formGroup = formBuilder.group({
      enableWifi: '',
      acceptTerms: ['', Validators.requiredTrue]
    });
  }

  ngOnInit() {
    this.userService.getUsers().then(response => {
      this.dataSource = new MatTableDataSource(response);
    })
    

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  eliminarUsuario(i: string) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '30%',
      height: '25%',
      data : { 
        title : "Eliminar",
        subtitle : "Estas seguro de eliminar este usuario ?",
        accion : "Eliminar",
        id : i
      }
    });
  }

  informacionUsuario(row: any) {
    const dialogRef = this.dialog.open(InformacionUsuarioComponent, {
      width: '50%',
      height: '65%',
      data : {
        crew : row.crew,
        nombre : row.nombre,
        rolInterbank : row.rolInterbank,
        usuario : row.email,
        permisos : row.role
      }
    });
  }

  agregarUsuario() {
    const dialogRef = this.dialog.open(AddUserComponent, {
      width: '50%',
      height: '65%'
    });
  }
}
