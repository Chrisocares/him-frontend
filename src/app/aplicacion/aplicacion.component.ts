import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Program } from '../models/program';
import { MatDialog } from '@angular/material';
import { AddProgramaDialogComponent } from '../dialogs/add-programa-dialog/add-programa-dialog.component';
import { InformacionProgramaDialogComponent } from '../dialogs/informacion-programa-dialog/informacion-programa-dialog.component';
import { EditProgramaDialogComponent } from '../dialogs/edit-programa-dialog/edit-programa-dialog.component';
import { ApplicationService } from '../service/application.service';
import { Programa } from '../models/programa';
import { InformacionServicioDialogComponent } from '../dialogs/informacion-servicio-dialog/informacion-servicio-dialog.component';
import { AuthService } from '../service/auth.service';
import { saveAs } from 'file-saver';

declare var treeBoxes: any;

@Component({
  selector: 'app-aplicacion',
  templateUrl: './aplicacion.component.html',
  styleUrls: ['./aplicacion.component.scss']
})

export class AplicacionComponent implements OnInit {
  list = new Array<Programa>();
  private items: Program[] = [];
  programHost : Program;
  idAplication: any;
  data: any; 
  tipoAplicativo : string;
  roleUser: string;

  constructor(
    public dialog: MatDialog,
    private http: HttpClient,
    private router:ActivatedRoute,
    private authService: AuthService,
    private applicationService: ApplicationService) {
  }

  ngOnInit() {
    /*this.router.params.subscribe(params => {
      this.applicationService.getApplication(params.id).then(response => {
        this.list = response.programas
        this.tipoAplicativo = response.tipo
      })
    });
    this.roleUser = this.authService.getUser().usuario.role*/
    var json = { "tree" : { "nodeName" : "NODE NAME 1", "name" : "NODE NAME 1", "type" : "type3", "code" : "N1", "label" : "Node name 1", "version" : "v1.0", "link" : { "name" : "Link NODE NAME 1", "nodeName" : "NODE NAME 1", "direction" : "ASYN" }, "children" : [{ "nodeName" : "NODE NAME 2.1", "name" : "NODE NAME 2.1", "type" : "type1", "code" : "N2.1", "label" : "Node name 2.1", "version" : "v1.0", "link" : { "name" : "Link node 1 to 2.1", "nodeName" : "NODE NAME 2.1", "direction" : "SYNC" }, "children" : [{ "nodeName" : "NODE NAME 3.1", "name" : "NODE NAME 3.1", "type" : "type2", "code" : "N3.1", "label" : "Node name 3.1", "version" : "v1.0", "link" : { "name" : "Link node 2.1 to 3.1", "nodeName" : "NODE NAME 3.1", "direction" : "SYNC" }, "children" : [{ "nodeName" : "NODE NAME 3.1", "name" : "NODE NAME 3.1", "type" : "type2", "code" : "N3.1", "label" : "Node name 3.1", "version" : "v1.0", "link" : { "name" : "Link node 2.1 to 3.1", "nodeName" : "NODE NAME 3.1", "direction" : "SYNC" }, "children" : [{ "nodeName" : "NODE NAME 3.1", "name" : "NODE NAME 3.1", "type" : "type2", "code" : "N3.1", "label" : "Node name 3.1", "version" : "v1.0", "link" : { "name" : "Link node 2.1 to 3.1", "nodeName" : "NODE NAME 3.1", "direction" : "SYNC" }, "children" : [{ "nodeName" : "NODE NAME 3.1", "name" : "NODE NAME 3.1", "type" : "type2", "code" : "N3.1", "label" : "Node name 3.1", "version" : "v1.0", "link" : { "name" : "Link node 2.1 to 3.1", "nodeName" : "NODE NAME 3.1", "direction" : "SYNC" }, "children" : [{ "nodeName" : "NODE NAME 3.1", "name" : "NODE NAME 3.1", "type" : "type2", "code" : "N3.1", "label" : "Node name 3.1", "version" : "v1.0", "link" : { "name" : "Link node 2.1 to 3.1", "nodeName" : "NODE NAME 3.1", "direction" : "SYNC" }, "children" : [{ "nodeName" : "NODE NAME 3.1", "name" : "NODE NAME 3.1", "type" : "type2", "code" : "N3.1", "label" : "Node name 3.1", "version" : "v1.0", "link" : { "name" : "Link node 2.1 to 3.1", "nodeName" : "NODE NAME 3.1", "direction" : "SYNC" }, "children" : [{ "nodeName" : "NODE NAME 3.1", "name" : "NODE NAME 3.1", "type" : "type2", "code" : "N3.1", "label" : "Node name 3.1", "version" : "v1.0", "link" : { "name" : "Link node 2.1 to 3.1", "nodeName" : "NODE NAME 3.1", "direction" : "SYNC" }, "children" : [{ "nodeName" : "NODE NAME 3.1", "name" : "NODE NAME 3.1", "type" : "type2", "code" : "N3.1", "label" : "Node name 3.1", "version" : "v1.0", "link" : { "name" : "Link node 2.1 to 3.1", "nodeName" : "NODE NAME 3.1", "direction" : "SYNC" }, "children" : [{ "nodeName" : "NODE NAME 3.1", "name" : "NODE NAME 3.1", "type" : "type2", "code" : "N3.1", "label" : "Node name 3.1", "version" : "v1.0", "link" : { "name" : "Link node 2.1 to 3.1", "nodeName" : "NODE NAME 3.1", "direction" : "SYNC" }, "children" : [] }] }] }] }] }] }] }] }] }, { "nodeName" : "NODE NAME 3.2", "name" : "NODE NAME 3.2", "type" : "type2", "code" : "N3.2", "label" : "Node name 3.2", "version" : "v1.0", "link" : { "name" : "Link node 2.1 to 3.2", "nodeName" : "NODE NAME 3.1", "direction" : "SYNC" }, "children" : [] } ] }, { "nodeName" : "NODE NAME 2.2", "name" : "NODE NAME 2.2", "type" : "type1", "code" : "N2.2", "label" : "Node name 2.2", "version" : "v1.0", "link" : { "name" : "Link node 1 to 2.2", "nodeName" : "NODE NAME 2.2", "direction" : "ASYN" }, "children" : [] }, { "nodeName" : "NODE NAME 2.3", "name" : "NODE NAME 2.3", "type" : "type1", "code" : "N2.3", "label" : "Node name 2.3", "version" : "v1.0", "link" : { "name" : "Link node 1 to 2.3", "nodeName" : "NODE NAME 2.3", "direction" : "ASYN" }, "children" : [{ "nodeName" : "NODE NAME 3.3", "name" : "NODE NAME 3.3", "type" : "type1", "code" : "N3.3", "label" : "Node name 3.3", "version" : "v1.0", "link" : { "name" : "Link node 2.3 to 3.3", "nodeName" : "NODE NAME 3.3", "direction" : "ASYN" }, "children" : [{ "nodeName" : "NODE NAME 4.1", "name" : "NODE NAME 4.1", "type" : "type4", "code" : "N4.1", "label" : "Node name 4.1", "version" : "v1.0", "link" : { "name" : "Link node 3.3 to 4.1", "nodeName" : "NODE NAME 4.1", "direction" : "SYNC" }, "children" : [] } ] }, { "nodeName" : "NODE NAME 3.4", "name" : "NODE NAME 3.4", "type" : "type1", "code" : "N3.4", "label" : "Node name 3.4", "version" : "v1.0", "link" : { "name" : "Link node 2.3 to 3.4", "nodeName" : "NODE NAME 3.4", "direction" : "ASYN", "link" : { "name" : "Link node 2.3 to 3.4", "nodeName" : "NODE NAME 3.4", "direction" : "ASYN", "link" : { "name" : "Link node 2.3 to 3.4", "nodeName" : "NODE NAME 3.4", "direction" : "ASYN", "link" : { "name" : "Link node 2.3 to 3.4", "nodeName" : "NODE NAME 3.4", "direction" : "ASYN", "link" : { "name" : "Link node 2.3 to 3.4", "nodeName" : "NODE NAME 3.4", "direction" : "ASYN", "link" : { "name" : "Link node 2.3 to 3.4", "nodeName" : "NODE NAME 3.4", "direction" : "ASYN" } } } } } }, "children" : [{ "nodeName" : "NODE NAME 4.2", "name" : "NODE NAME 4.2", "type" : "type4", "code" : "N4.2", "label" : "Node name 4.2", "version" : "v1.0", "link" : { "name" : "Link node 3.4 to 4.2", "nodeName" : "NODE NAME 4.1", "direction" : "ASYN" }, "children" : [] } ] } ] } ] } };
    new treeBoxes('',json.tree)
  }
/*
  obtenerEntrada(item: object){
    if (this.tipoAplicativo == "1") {
      const dialogRef = this.dialog.open(InformacionProgramaDialogComponent, {
        width: '500px',
        height: '500px',
        data : { 
          object : item,
          identifier : "IN"
        }
      });
    } else if (this.tipoAplicativo == "2") {
      const dialogRef = this.dialog.open(InformacionServicioDialogComponent, {
        width: '500px',
        height: '500px',
        data : { 
          object : item,
          identifier : "IN"
        }
      });
    }
  }

  obtenerSalida(item: object){
    if (this.tipoAplicativo == "1") {
      const dialogRef = this.dialog.open(InformacionProgramaDialogComponent, {
        width: '500px',
        height: '500px',
        data : { 
          object : item,
          identifier : "OUT"
        }
      });
    } else if (this.tipoAplicativo == "2") {
      const dialogRef = this.dialog.open(InformacionServicioDialogComponent, {
        width: '500px',
        height: '500px',
        data : { 
          object : item,
          identifier : "OUT"
        }
      });
    }
  }

  descargar(nombre: string) {
    this.applicationService.getProgramsDownload().then(response => {
      saveAs(response, nombre+".txt");
    }).catch(e => {
      console.log(e);
   });
  }

  editarPrograma(item: any){
    const dialogRef = this.dialog.open(EditProgramaDialogComponent, {
      width: '800px',
      height: '600px',
      data : { 
        programa: item.programa,
        funcionalidad : item.funcionalidad,
        api: item.api,
        entrada: item.entrada,
        salida: item.salida,
        bus_codigo: item.bus_codigo,
        bus_version: item.bus_version,
        bus_framework: item.bus_framework,
        bus_identificador: item.bus_identificador
      }
    });
  }

  agregarPrograma(){
    const dialogRef = this.dialog.open(AddProgramaDialogComponent, {
      width: '800px',
      height: '600px'
    });
  }*/

}
