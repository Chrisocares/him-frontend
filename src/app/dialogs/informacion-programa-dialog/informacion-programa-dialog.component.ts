import { Component, OnInit, Inject } from '@angular/core';
import { Program } from 'src/app/models/program';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { Programa } from 'src/app/models/programa';

@Component({
  selector: 'app-informacion-programa-dialog',
  templateUrl: './informacion-programa-dialog.component.html',
  styleUrls: ['./informacion-programa-dialog.component.css']
})
export class InformacionProgramaDialogComponent {
  title : any;
  object : string;
  constructor(public dialogRef: MatDialogRef<InformacionProgramaDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  aplicacionFormControl: FormGroup

  ngOnInit(): void {
    console.log(JSON.stringify(this.data.object.entrada))
    if (this.data.identifier == "IN") {
      this.object = this.data.object.entrada
      this.title = "ENTRADA";
    } else if (this.data.identifier == "OUT") {
      this.object = this.data.object.salida
      this.title = "SALIDA";
    }
    
    /*
    this.aplicacionFormControl = new FormGroup({
      bus_codigo: new FormControl(this.data.bus_codigo),
      bus_version: new FormControl(this.data.bus_version),
      bus_framework: new FormControl(this.data.bus_framework),
      bus_identificador: new FormControl(this.data.bus_identificador)
    })*/
  }
}
