import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit, EventEmitter} from '@angular/core';
import {FormControl, Validators, FormGroup} from '@angular/forms';
import {Aplication} from '../../models/aplication';

@Component({
  selector: 'app-informacion.dialog',
  templateUrl: '../../dialogs/informacion/informacion.dialog.html',
  styleUrls: ['../../dialogs/informacion/informacion.dialog.css']
})

export class InformacionDialogComponent {
  
  aplicacionFormControl: FormGroup

  constructor(public dialogRef: MatDialogRef<InformacionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Aplication) { }
    
}
