import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformacionServicioDialogComponent } from './informacion-servicio-dialog.component';

describe('InformacionServicioDialogComponent', () => {
  let component: InformacionServicioDialogComponent;
  let fixture: ComponentFixture<InformacionServicioDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacionServicioDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformacionServicioDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
