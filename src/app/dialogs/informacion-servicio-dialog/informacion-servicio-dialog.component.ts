import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-informacion-servicio-dialog',
  templateUrl: './informacion-servicio-dialog.component.html',
  styleUrls: ['./informacion-servicio-dialog.component.css']
})
export class InformacionServicioDialogComponent implements OnInit {
  xml : string;
  tittle : any;
  object : string;
  constructor(
    public dialogRef: MatDialogRef<InformacionServicioDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    if (this.data.identifier == "IN") {
      this.object = this.data.object.salida
      this.tittle = "Request";
    } else if (this.data.identifier == "OUT") {
      this.object = this.data.object.salida
      this.tittle = "Response";
    }
  }

}
