import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteProgramaDialogComponent } from './delete-programa-dialog.component';

describe('DeleteProgramaDialogComponent', () => {
  let component: DeleteProgramaDialogComponent;
  let fixture: ComponentFixture<DeleteProgramaDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteProgramaDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteProgramaDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
