import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, EventEmitter} from '@angular/core';
import {FormControl, Validators, FormGroup} from '@angular/forms';
import { Program } from 'src/app/models/program';

@Component({
  selector: 'app-edit-programa-dialog',
  templateUrl: './edit-programa-dialog.component.html',
  styleUrls: ['./edit-programa-dialog.component.css']
})
export class EditProgramaDialogComponent {

  constructor(public dialogRef: MatDialogRef<EditProgramaDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  programaFormControl: FormGroup
  onAdd = new EventEmitter();

  ngOnInit(): void {
    this.programaFormControl = new FormGroup({
      programa: new FormControl(this.data.programa),
      funcionalidad: new FormControl(this.data.funcionalidad),
      api: new FormControl(this.data.api),
      entrada: new FormControl(this.data.entrada),
      salida: new FormControl(this.data.salida),
      bus_codigo: new FormControl(this.data.bus_codigo),
      bus_version: new FormControl(this.data.bus_version),
      bus_framework: new FormControl(this.data.bus_framework),
      bus_identificador: new FormControl(this.data.bus_identificador)
    })
  }

  submit() {
    this.programaFormControl.value.id = this.data.id
    this.programaFormControl.value.id_aplicacion = this.data.id_aplicacion
    this.data = <Program>this.programaFormControl.value
    this.onAdd.emit(this.data);
  }

}
