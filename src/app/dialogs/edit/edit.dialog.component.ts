import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, EventEmitter} from '@angular/core';
import {FormControl, Validators, FormGroup} from '@angular/forms';
import { Aplication } from 'src/app/models/aplication';

@Component({
  selector: 'app-baza.dialog',
  templateUrl: '../../dialogs/edit/edit.dialog.html',
  styleUrls: ['../../dialogs/edit/edit.dialog.css']
})
export class EditDialogComponent {

  constructor(public dialogRef: MatDialogRef<EditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  aplicacionFormControl: FormGroup
  onAdd = new EventEmitter();

  ngOnInit(): void {
    this.aplicacionFormControl = new FormGroup({
      aplicacion: new FormControl(this.data.aplicacion,Validators.required),
      objetivo: new FormControl(this.data.objetivo,Validators.required),
      custodio: new FormControl(this.data.custodio,Validators.required),
      plataforma: new FormControl(this.data.plataforma,Validators.required),
      funcionalidad: new FormControl(this.data.funcionalidad,Validators.required),
      informacion: new FormControl(this.data.informacion,Validators.required)
    })  
  }

  submit() {
    this.aplicacionFormControl.value.id = this.data.id
    this.data = <Aplication>this.aplicacionFormControl.value
    console.log(this.data.id)
    this.onAdd.emit(this.data);
  }


}
