import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { UserService } from 'src/app/service/user.service';
import { ConfirmacionDialogComponent } from '../confirmacion-dialog/confirmacion-dialog.component';

interface TypesPermissions{
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  public isSubmitting = false;
  usuarioFormControl: FormGroup;
  typesPermissions: TypesPermissions[] = [
    {value: 'ADMIN_ROLE', viewValue: 'VISTA'},
    {value: 'USER_ROLE', viewValue: 'VISTA ESCRITURA'},
    {value: 'ORG_ADMIN_ROLE', viewValue: 'ORG ADMIN'},
  ];

  constructor(
    public dialog: MatDialog,
    formBuilder: FormBuilder,
    public userService: UserService,
    public dialogRef: MatDialogRef<AddUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.usuarioFormControl = formBuilder.group({
      nombre: [ '', Validators.required],
      crew: [ '', Validators.required],
      rol: [ '', Validators.required],
      usuario: [ '', Validators.required],
      permisos: [ '', Validators.required],
      password: [ '', Validators.required]
    });
  }

  ngOnInit(): void {
    this.usuarioFormControl = new FormGroup({
      nombre: new FormControl(""),
      crew: new FormControl(""),
      usuario: new FormControl(""),
      rol: new FormControl(""),
      permisos: new FormControl(""),
      password: new FormControl("")
    })
  }

  onSubmit() {
    this.isSubmitting = true;
    this.userService
      .register(this.usuarioFormControl.value)
      .then(user => {
        this.isSubmitting = false;
        this.dialogRef.close();
        this.dialog.open(ConfirmacionDialogComponent, {
          width: '30%',
          height: '25%',
          data : {
            title : "Usuario",
            subtitle : "Se creo el usuario exitosamente",
            accion : "Cerrar"
          }
        });
      })
      .catch(error => {
        this.isSubmitting = false;
        //this.error = error;
      });
  }

  selectTypePermissions(i: string){
    this.usuarioFormControl.value.permisos = i;
  }

}
