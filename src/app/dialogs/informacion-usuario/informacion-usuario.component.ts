import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-informacion-usuario',
  templateUrl: './informacion-usuario.component.html',
  styleUrls: ['./informacion-usuario.component.css']
})
export class InformacionUsuarioComponent implements OnInit {

  usuarioFormControl: FormGroup;

  constructor(public dialogRef: MatDialogRef<InformacionUsuarioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.usuarioFormControl = new FormGroup({
      nombre: new FormControl(this.data.nombre),
      crew: new FormControl(this.data.crew),
      usuario: new FormControl(this.data.usuario),
      rol: new FormControl(this.data.rolInterbank),
      permisos: new FormControl(this.data.permisos)
    })
    this.usuarioFormControl.disable()
  }

}
