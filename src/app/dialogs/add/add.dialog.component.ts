import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit, EventEmitter} from '@angular/core';
import {FormControl, Validators, FormGroup} from '@angular/forms';
import {Aplication} from '../../models/aplication';

@Component({
  selector: 'app-add.dialog',
  templateUrl: '../../dialogs/add/add.dialog.html',
  styleUrls: ['../../dialogs/add/add.dialog.css']
})

export class AddDialogComponent implements OnInit{
  aplicacionFormControl: FormGroup
  onAdd = new EventEmitter();
  ngOnInit(): void {
    this.aplicacionFormControl = new FormGroup({
      aplicacion: new FormControl('',Validators.required),
      objetivo: new FormControl('',Validators.required),
      custodio: new FormControl('',Validators.required),
      plataforma: new FormControl('',Validators.required),
      funcionalidad: new FormControl('',Validators.required),
      informacion: new FormControl('',Validators.required)
    })  
  }

  constructor(public dialogRef: MatDialogRef<AddDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Aplication) { }

  submit() {
    this.data = <Aplication>this.aplicacionFormControl.value
    this.onAdd.emit(this.data);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
