import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material';
import {Component, Inject} from '@angular/core';
import { UserService } from 'src/app/service/user.service';
import { ConfirmacionDialogComponent } from '../confirmacion-dialog/confirmacion-dialog.component';


@Component({
  selector: 'app-delete.dialog',
  templateUrl: '../../dialogs/delete/delete.dialog.html',
  styleUrls: ['../../dialogs/delete/delete.dialog.css']
})
export class DeleteDialogComponent {

  constructor(
    public dialog: MatDialog,
    public userService: UserService,
    public dialogRef: MatDialogRef<DeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.userService.deleteUser(this.data.id);
    this.dialogRef.close();
    this.dialog.open(ConfirmacionDialogComponent, {
      width: '30%',
      height: '25%',
      data : {
        title : "Eliminar",
        subtitle : "Se elimino el usuario exitosamente",
        accion : "Cerrar"
      }
    });
  }
}
