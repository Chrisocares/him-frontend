import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { Component, Inject, OnInit, EventEmitter } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Program } from 'src/app/models/program';
import { ApplicationService } from 'src/app/service/application.service';
import { ConfirmacionDialogComponent } from '../confirmacion-dialog/confirmacion-dialog.component';

@Component({
  selector: 'app-add-programa-dialog',
  templateUrl: './add-programa-dialog.component.html',
  styleUrls: ['./add-programa-dialog.component.css']
})
export class AddProgramaDialogComponent implements OnInit {
  aplicacionFormControl: FormGroup
  onAdd = new EventEmitter();

  ngOnInit(): void {
    this.aplicacionFormControl = new FormGroup({
      programa: new FormControl('', Validators.required),
      funcionalidad: new FormControl('', Validators.required),
      entrada: new FormControl('', Validators.required),
      salida: new FormControl('', Validators.required),
      bus_codigo: new FormControl('', Validators.required),
      bus_version: new FormControl('', Validators.required),
      bus_framework: new FormControl('', Validators.required),
      bus_identificador: new FormControl('', Validators.required),
      api: new FormControl('', Validators.required),
      new : new FormControl(false)
    })
  }

  constructor(
    public aplicationService: ApplicationService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<AddProgramaDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Program) { }

  submit() {
    /*this.data = <Program>this.aplicacionFormControl.value
    this.onAdd.emit(this.data);*/
    this.data = <Program>this.aplicacionFormControl.value;
    console.log(JSON.stringify(this.data))
    this.aplicationService.postProgram("5eef0c5aef62490012902e70",this.data).then(response => {
      console.log(JSON.stringify(response))
      const dialogRef = this.dialog.open(ConfirmacionDialogComponent, {
        width: '30%',
        height: '25%',
        data: {
          title: "Programa",
          subtitle: "Se creo exitosamente el programa",
        }
      });
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onFileComplete(data: any) {
    console.log("complete" + data);
  }

}
