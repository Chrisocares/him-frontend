import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { HttpClient, HttpResponse, HttpRequest, 
         HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { catchError, last, map, tap } from 'rxjs/operators';
import { of, Subscription } from 'rxjs';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css'],
  animations: [
    trigger('fadeInOut', [
          state('in', style({ opacity: 100 })),
          transition('* => void', [
                animate(300, style({ opacity: 0 }))
          ])
    ])
]
})
export class FileUploadComponent implements OnInit {
  
  file:any;
  @Input() text = 'Copy';
  @Input() param = 'file';
  @Input() accept = 'image/*';
  @Output() complete = new EventEmitter();

  private files: Array<FileUploadModel> = [];

  constructor(private _http: HttpClient) { }

  ngOnInit() {
  }

  onClick(e) {
        const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
        fileUpload.onchange = () => {
                  var file = fileUpload.files[0];
                  var fileReader = new FileReader();
                  fileReader.onload = this.processFile(file);
                  fileReader.readAsText(file); 
        };
        fileUpload.click();
  }

  processFile(theFile){
      return function(e)  { 
        var theBytes = e.target.result;
        let programa = JSON.parse(theBytes);
        localStorage.setItem(programa.programa, programa.copy)
      }
    }

}

export class FileUploadModel {
  data: File;
  state: string;
  inProgress: boolean;
  progress: number;
  canRetry: boolean;
  canCancel: boolean;
  sub?: Subscription;
}