import { Component, OnInit, ɵConsole } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

export interface Copy {
  programa: string;
  copy: string; 
}

@Component({
  selector: 'app-programa',
  templateUrl: './programa.component.html',
  styleUrls: ['./programa.component.css']
})
export class ProgramaComponent implements OnInit {
  data: Copy; 
  aplicacion:  any;
  nombre_copy: any;

    
  constructor(private http: HttpClient, private router:ActivatedRoute) { }

  ngOnInit() {
    this.router.params.subscribe(params => {
      this.nombre_copy = params.nombre_copy
    });
    this.data = {programa: "", copy:""}
    this.readData(); 
  }

  readData(){
    const jsonFile = `assets/data/`+this.nombre_copy+`.json`;

    return new Promise<void>((resolve, reject) => {
        this.http.get(jsonFile).toPromise().then((response : Copy) => {
          this.data = <Copy>response;
            resolve();
        }).catch((response: any) => {
            if(localStorage.getItem(this.nombre_copy) != null){
              const copy: Copy = {
                programa: this.nombre_copy,
                copy: localStorage.getItem(this.nombre_copy)
              }
              this.data = copy
              resolve();
            } else {
              console.log("no existe")
            }
        });
    });
  }
}
